﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    int turn;

    int players;

    public Color blue;
    public Color red;
    public Color green;
    public Color yellow;


    public string player1;
    public string player2;
    public string player3;
    public string player4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    player1 = GameObject.Find("GameController").GetComponent<SelectToken1>().player1;
        player2 = GameObject.Find("GameController").GetComponent<SelectToken1>().player2;
        player3 = GameObject.Find("GameController").GetComponent<SelectToken1>().player3;
        player4 = GameObject.Find("GameController").GetComponent<SelectToken1>().player4;

        turn = GameObject.Find("GameController").GetComponent<SelectToken1>().turn;

        if (this.gameObject.name.Contains("TurnName"))
        {
            switch (turn)
            {
                case 1:
                    this.gameObject.GetComponent<Text>().text = player1;
                    this.gameObject.GetComponent<Text>().color = blue;
                    break;
                case 2:
                    this.gameObject.GetComponent<Text>().text = player2;
                    this.gameObject.GetComponent<Text>().color = red;
                    break;
                case 3:
                    this.gameObject.GetComponent<Text>().text = player3;
                    this.gameObject.GetComponent<Text>().color = green;
                    break;
                case 4:
                    this.gameObject.GetComponent<Text>().text = player4;
                    this.gameObject.GetComponent<Text>().color = yellow;
                    break;

            }
        }

        if (this.gameObject.name.Contains("PlayerSliderNum"))
        {
            this.gameObject.GetComponent<Text>().text = GetComponentInParent<Slider>().value.ToString();
            players = (int)GetComponentInParent<Slider>().value;
        }


    }
}
