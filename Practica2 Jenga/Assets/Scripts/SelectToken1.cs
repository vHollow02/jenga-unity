﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectToken1 : MonoBehaviour
{
    public bool fall;



    public bool moveToken;
    public bool moveRotatedToken;
    RaycastHit objectHit = new RaycastHit();
    public int randomNumber;
    public int randomDirection;
    public int nextPosition;
    public int gameMode;

    bool diceRoll;
    public int playerNum;

    public GameObject camera;

    int camHeight;

    public GameObject selectPos;
    public GameObject selectPosRotated;

    public GameObject name1;
    public GameObject name2;
    public GameObject name3;
    public GameObject name4;

    public bool token1;
    public bool token2;
    public bool token3;
    public bool token4;
    public bool token5;
    public bool token6;

    public string player1;
    public string player2;
    public string player3;
    public string player4;

    public GameObject pos1;
    public GameObject pos2;
    public GameObject pos3;
    public GameObject pos4;
    public GameObject pos5;
    public GameObject pos6;

    public GameObject easy;
    public GameObject medium;
    public GameObject hard;

    public GameObject GameUI;

    public Material blue;
    public Material red;
    public Material green;
    public Material yellow;

    public int turn;
    public int towerHeight;

    public GameObject dice;
    public bool diceGame;

    public int nextColor;

    public GameObject menu;

    // Start is called before the first frame update
    void Start()
    {
        towerHeight = 14;
        playerNum = 2;
    }

    public void Dice()
    {
        if(diceGame == true)
        {
            diceGame = false;
        }
        else
        {
            diceGame = true;
        }
    }

    public void Play()
    {        
        player1 = GameObject.Find("Player1NameText").GetComponent<Text>().text;
        player2 = GameObject.Find("Player2NameText").GetComponent<Text>().text;
        if (playerNum == 3)
        {
            player3 = GameObject.Find("Player3NameText").GetComponent<Text>().text;
        }
        if (playerNum == 4)
        {
            player4 = GameObject.Find("Player4NameText").GetComponent<Text>().text;
        }
        menu.SetActive(false);
        GameUI.SetActive(true);

        if(player1 == "") 
        {
            player1 = "jugador1"; 
        }

        if (player2 == "")
        {
            player2 = "jugador2";
        }

        if (player3 == "")
        {
            player3 = "jugador3";
        }


        if (player4 == "")
        {
            player4 = "jugador4";
        }

    }

    public void ChangeHeightSmall()
    {
        towerHeight = 12;
        easy.SetActive(false);
        medium.SetActive(false);
        hard.SetActive(false);
    }
    public void ChangeHeightMed()
    {
        towerHeight = 14;
        easy.SetActive(true);
        medium.SetActive(false);
        hard.SetActive(false);
    }

    public void ChangeHeightBig()
    {
        towerHeight = 16;
        easy.SetActive(true);
        medium.SetActive(true);
        hard.SetActive(false);
    }

    public void ChangeHeightGiant()
    {
        towerHeight = 18;
        easy.SetActive(true);
        medium.SetActive(true);
        hard.SetActive(true);
    }

    public void reloadNum()
    {
        playerNum = (int)GameObject.Find("PlayerSlider").GetComponent<Slider>().value;
    }

    void Update() {

        if(fall == true)
        {
            StopAllCoroutines();
        }
       

        if (dice.transform.eulerAngles.x >= -0.5 & dice.transform.eulerAngles.x <= 0.5 || dice.transform.eulerAngles.x >= 179.5 & dice.transform.eulerAngles.x <= 180.5)
        {
            
            if(dice.transform.eulerAngles.z >= -0.5 & dice.transform.eulerAngles.z <= 0.5 || dice.transform.eulerAngles.z >= 179.5 & dice.transform.eulerAngles.z <= 180.5)
            {
                nextColor = 1; //naranja
            }
            if (dice.transform.eulerAngles.z == 90 || dice.transform.eulerAngles.z == 270 || dice.transform.eulerAngles.z == -270 || dice.transform.eulerAngles.z == -90)
            {
                nextColor = 2; //marron
            }
        }

        else if (dice.transform.eulerAngles.x >= 90 || dice.transform.eulerAngles.x <= -90 || dice.transform.eulerAngles.x == 270 || dice.transform.eulerAngles.x == -270)
        {
            if (dice.transform.eulerAngles.z >= -0.5 & dice.transform.eulerAngles.z <= 0.5 || dice.transform.eulerAngles.z >= 179.5 & dice.transform.eulerAngles.z <= 180.5)
            {
                nextColor = 3; //claro
            }           
        }

        else
        {
            nextColor = 0;
        }


        if (GameObject.Find("HeightIndicator") != null){
            GameObject.Find("HeightIndicator").GetComponent<Text>().text = towerHeight.ToString();
        }
    
        switch (playerNum)
        {
            case 1:                
                name1.SetActive(true);
                name2.SetActive(false);
                name3.SetActive(false);
                name4.SetActive(false);
                break;
            case 2:
                name2.SetActive(true);
                name1.SetActive(true);
                name3.SetActive(false);
                name4.SetActive(false);
                break;
            case 3:
                name1.SetActive(true);                
                name2.SetActive(true);                
                name3.SetActive(true);                
                name4.SetActive(false);
                break;
            case 4:
                name1.SetActive(true);               
                name2.SetActive(true);                
                name3.SetActive(true);               
                name4.SetActive(true);
                break;
        }

        if (diceGame == true)
        {
            dice.SetActive(true);           
        }
        else { dice.SetActive(false); }


        if (turn > playerNum)
        {
            turn = 1;
        }     
        
        
        camera.transform.position = new Vector3(camera.transform.position.x, towerHeight+20, camera.transform.position.z);

        if (moveToken == false & moveRotatedToken == false & GameObject.Find("Menu") == null) { 
            if (Input.GetMouseButtonDown(0))      
            {                    
                bool checkForHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out objectHit);
           
                if (checkForHit == true) {
                    
                    if (objectHit.transform.gameObject.tag == "Dice" & GameObject.Find("Select(Clone)") == null & GameObject.Find("SelectRotated(Clone)") == null & diceRoll == false)
                    {
                        if (objectHit.transform.position.y < 10)
                        {
                            diceRoll = true;
                            objectHit.transform.position = new Vector3(-20, 50, -10);
                            objectHit.transform.rotation = Quaternion.Euler(Random.Range(0, 90), Random.Range(0, 90), Random.Range(0, 90));
                        }                   

                    }

                    if (objectHit.transform.gameObject.tag == "Token" & GameObject.Find("Select(Clone)") == null & GameObject.Find("SelectRotated(Clone)") == null)
                    {
                        if (diceGame == true)
                        {
                            if ((nextColor == 1 & objectHit.transform.gameObject.name.Contains("Right_Token")) || (nextColor == 2 & objectHit.transform.gameObject.name.Contains("Center_token")) || (nextColor == 3 & objectHit.transform.gameObject.name.Contains("Left_Token")))
                            {
                                moveToken = true;
                                Transform childObj = objectHit.transform.Find("hand");
                                childObj.gameObject.SetActive(true);
                                switch (turn)
                                {
                                    case 1:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = blue;
                                        break;
                                    case 2:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = red;
                                        break;
                                    case 3:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = green;
                                        break;
                                    case 4:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = yellow;
                                        break;
                                }
                                randomNumber = Random.Range(10, 20);
                                randomDirection = Random.Range(1, 2);
                                objectHit.transform.gameObject.tag = "Untagged";
                            }
                        }
                        else
                        {
                            moveToken = true;
                            Transform childObj = objectHit.transform.Find("hand");
                            childObj.gameObject.SetActive(true);
                            switch (turn)
                            {
                                case 1:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = blue;
                                    break;
                                case 2:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = red;
                                    break;
                                case 3:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = green;
                                    break;
                                case 4:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = yellow;
                                    break;
                            }
                            randomNumber = Random.Range(10, 20);
                            randomDirection = Random.Range(1, 2);
                            objectHit.transform.gameObject.tag = "Untagged";
                        }
                    }

                    if (objectHit.transform.gameObject.tag == "RotatedToken" & GameObject.Find("Select(Clone)") == null & GameObject.Find("SelectRotated(Clone)") == null)
                    {
                        if (diceGame == true)
                        {
                            if ((nextColor == 1 & objectHit.transform.gameObject.name.Contains("Right_Token")) || (nextColor == 2 & objectHit.transform.gameObject.name.Contains("Center_token")) || (nextColor == 3 & objectHit.transform.gameObject.name.Contains("Left_Token")))
                            {
                                randomDirection = Random.Range(1, 2);
                                Transform childObj = objectHit.transform.Find("hand");
                                childObj.gameObject.SetActive(true);
                                moveRotatedToken = true;
                                switch (turn)
                                {
                                    case 1:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = blue;
                                        break;
                                    case 2:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = red;
                                        break;
                                    case 3:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = green;
                                        break;
                                    case 4:
                                        childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = yellow;
                                        break;
                                }
                                randomNumber = Random.Range(10, 20);
                                objectHit.transform.gameObject.tag = "Untagged";
                            }
                        }
                        else
                        {
                            randomDirection = Random.Range(1, 2);
                            Transform childObj = objectHit.transform.Find("hand");
                            childObj.gameObject.SetActive(true);
                            moveRotatedToken = true;
                            switch (turn)
                            {
                                case 1:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = blue;
                                    break;
                                case 2:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = red;
                                    break;
                                case 3:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = green;
                                    break;
                                case 4:
                                    childObj.GetComponentInChildren<SkinnedMeshRenderer>().material = yellow;
                                    break;
                            }
                            randomNumber = Random.Range(10, 20);
                            objectHit.transform.gameObject.tag = "Untagged";
                        }
                    }

                    if (objectHit.transform.gameObject.tag == "pos1")
                    {
                        if (token1 == false)
                        {
                            Instantiate(pos1);
                            StartCoroutine("Turn");                             
                            nextPosition = nextPosition + 1;
                            Destroy(GameObject.Find("Select(Clone)"));
                            token1 = true;
                        }
                    }
                    if (objectHit.transform.gameObject.tag == "pos2")
                    {
                        if (token2 == false)
                        {
                            Instantiate(pos2);
                            StartCoroutine("Turn");
                            nextPosition = nextPosition + 1;
                            Destroy(GameObject.Find("Select(Clone)"));
                            token2 = true;
                        }
                    }
                    if (objectHit.transform.gameObject.tag == "pos3")
                    {
                        if (token3 == false)
                        {
                            Instantiate(pos3);
                            StartCoroutine("Turn");
                            nextPosition = nextPosition + 1;
                            Destroy(GameObject.Find("Select(Clone)"));
                            token3 = true;
                        }
                    }
                    if (objectHit.transform.gameObject.tag == "pos4")
                    {
                        if (token4 == false)
                        {
                            Instantiate(pos4);
                            StartCoroutine("Turn");
                            nextPosition = nextPosition + 1;
                            Destroy(GameObject.Find("SelectRotated(Clone)"));
                            token4 = true;
                        }
                    }
                    if (objectHit.transform.gameObject.tag == "pos5")
                    {
                        if (token5 == false)
                        {
                            Instantiate(pos5);
                            StartCoroutine("Turn");
                            nextPosition = nextPosition + 1;
                            Destroy(GameObject.Find("SelectRotated(Clone)"));
                            token5 = true;
                        }
                    }
                    if (objectHit.transform.gameObject.tag == "pos6")
                    {
                        if (token6 == false)
                        {
                            Instantiate(pos6);
                            StartCoroutine("Turn");
                            nextPosition =  nextPosition + 1;
                            token6 = true;
                            Destroy(GameObject.Find("SelectRotated(Clone)"));                          
                        }
                    }

                    if(nextPosition == 7)
                    {
                        nextPosition = 1;
                        token1 = false;
                        token2 = false;
                        token3 = false;
                        token4 = false;
                        token5 = false;
                        token6 = false;
                        towerHeight = towerHeight + 2;
                    }

                    


                }
            }
        }


        if (moveToken == true)
        {
            switch (randomDirection) {
                case 1:
                    if (objectHit.transform.position.z > -randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y, objectHit.transform.position.z - 0.1f);
                        }
                    }        
                    else
                    {                       
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else 
                            {                          
                                switch (nextPosition) { 
                                    case 1:
                                            Instantiate(selectPos); 
                                            moveToken = false;
                                            break;
                                    case 2:
                                            Instantiate(selectPos);
                                            moveToken = false;                                            
                                            break;
                                        case 3:
                                            Instantiate(selectPos);
                                            moveToken = false;
                                            break;
                                        case 4:
                                            Instantiate(selectPosRotated);
                                            moveToken = false;
                                            break;
                                        case 5:
                                            Instantiate(selectPosRotated);                                        
                                            moveToken = false;
                                            break;                                            
                                    case 6:
                                            Instantiate(selectPosRotated);
                                            moveToken = false;                                        
                                            break;
                                }
                                Destroy(objectHit.transform.gameObject);
                                moveToken = false;
                            }
                        }
                        else
                        {
                            moveToken = false; 
                        }
                    }
                    break;

                case 2:
                    if (objectHit.transform.position.z < randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y, objectHit.transform.position.z + 0.1f);
                        }
                    }
                    else
                    {
                        
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                switch (nextPosition)
                                {
                                    case 1:
                                        Instantiate(selectPos);                                        
                                        moveToken = false;
                                        break;
                                    case 2:
                                        Instantiate(selectPos);                                        
                                        moveToken = false;
                                        break;
                                    case 3:
                                        Instantiate(selectPos);                                        
                                        moveToken = false;
                                        break;
                                    case 4:
                                        Instantiate(selectPosRotated);                                        
                                        moveToken = false;
                                        break;
                                    case 5:
                                        Instantiate(selectPosRotated);                                       
                                        moveToken = false;
                                        break;
                                    case 6:
                                        Instantiate(selectPosRotated);                                       
                                        moveToken = false;
                                        break;
                                }
                                Destroy(objectHit.transform.gameObject);
                                moveToken = false;
                            }                            
                        }
                        else
                        {
                            moveToken = false; 
                        }
                    }
                    break;
            }
        }


        if (moveRotatedToken == true)
        {
            switch (randomDirection)
            {
                case 1:
                    if (objectHit.transform.position.x > -randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x - 0.1f, objectHit.transform.position.y, objectHit.transform.position.z );
                        }
                    }
                    else
                    {
                       
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;

                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                
                                switch (nextPosition)
                                {
                                    case 1:
                                        Instantiate(selectPos);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 2:
                                        Instantiate(selectPos);                                        
                                        moveRotatedToken = false;
                                        break;
                                    case 3:
                                        Instantiate(selectPos);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 4:
                                        Instantiate(selectPosRotated);                                        
                                        moveRotatedToken = false;
                                        break;
                                    case 5:
                                        Instantiate(selectPosRotated);                                        
                                        moveRotatedToken = false;
                                        break;
                                    case 6:
                                        Instantiate(selectPosRotated);                                        
                                        moveRotatedToken = false;
                                        break;
                                }
                                Destroy(objectHit.transform.gameObject);
                                moveRotatedToken = false;
                            }
                        }
                        else
                        {
                            moveRotatedToken = false;
                        }
                    }
                    break;

                case 2:
                    if (objectHit.transform.position.x < randomNumber)
                    {
                        {
                            objectHit.transform.position = new Vector3(objectHit.transform.position.x + 0.1f, objectHit.transform.position.y, objectHit.transform.position.z);
                        }
                    }
                    else
                    {
                        if (gameMode == 1)
                        {
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                            objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = false;
                            if (objectHit.transform.position.y < 40)
                            {
                                {
                                    objectHit.transform.position = new Vector3(objectHit.transform.position.x, objectHit.transform.position.y + 0.5f, objectHit.transform.position.z);
                                }
                            }
                            else
                            {
                                objectHit.transform.gameObject.GetComponent<Rigidbody>().useGravity = true;
                                switch (nextPosition)
                                {
                                    case 1:
                                        Instantiate(selectPos);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 2:
                                        Instantiate(selectPos);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 3:
                                        Instantiate(selectPos);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 4:
                                        Instantiate(selectPosRotated);                                        
                                        moveRotatedToken = false;
                                        break;
                                    case 5:
                                        Instantiate(selectPosRotated);                                       
                                        moveRotatedToken = false;
                                        break;
                                    case 6:
                                        Instantiate(selectPosRotated);                                       
                                        moveRotatedToken = false;
                                        break;
                                }
                                Destroy(objectHit.transform.gameObject);
                                moveRotatedToken = false;
                            }
                        }
                        else
                        {
                            moveRotatedToken = false;
                        }
                    }
                    break;
            }
        }

    }
    

    IEnumerator Turn()
    {
        print("a");
        yield return new WaitForSeconds(3f);
        turn = turn + 1;
        diceRoll = false;
    }

    public void Back()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

  
}
